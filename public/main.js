var s1 = document.getElementById('s1');
var s2 = document.getElementById('s2');
var s3 = document.getElementById('s3');
var s4 = document.getElementById('s4');

var i1 = document.getElementById('i1');
var i2 = document.getElementById('i2');
var i3 = document.getElementById('i3');
var i4 = document.getElementById('i4');

var hot = document.getElementById('hot');

s1.oninput = function() {
  let burners = i1.getElementsByTagName("div");
  for (var i = 0; i < burners.length; i++) {
    burners[i].style.opacity = s1.value / 100;
  }
  checkHot();
}

s2.oninput = function() {
  let burners = i2.getElementsByTagName("div");
  for (var i = 0; i < burners.length; i++) {
    burners[i].style.opacity = s2.value / 100;
  }
  checkHot();
}

s3.oninput = function() {
  let burners = i3.getElementsByTagName("div");
  for (var i = 0; i < burners.length; i++) {
    burners[i].style.opacity = s3.value / 100;
  }
  checkHot();
}

s4.oninput = function() {
  let burners = i4.getElementsByTagName("div");
  for (var i = 0; i < burners.length; i++) {
    burners[i].style.opacity = s4.value / 100;
  }
  checkHot();
}

function checkHot() {
  if (s1.value + s2.value + s3.value + s4.value > 0) {
    hot.style.display = "block";
  } else {
    hot.style.display = "none";
  }
}
